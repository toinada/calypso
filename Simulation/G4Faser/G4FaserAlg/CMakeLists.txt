################################################################################
# Package: G4FaserAlg
################################################################################

# Declare the package name:
atlas_subdir( G4FaserAlg )

# External dependencies:
find_package( CLHEP )
find_package( Geant4 )
find_package( XercesC )
find_package( Eigen )

# G4AtlasAlgLib library

atlas_add_library( G4FaserAlgLib
                     src/*.cxx
                     NO_PUBLIC_HEADERS
                     INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS}
                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${EIGEN_LIBRARIES} ${XERCESC_LIBRARIES} ${CLHEP_LIBRARIES} AtlasHepMCLib G4AtlasAlgLib AthenaBaseComps AthenaKernel GaudiKernel G4AtlasInterfaces SGTools StoreGateLib SGtests EventInfo GeneratorObjects GeoModelInterfaces FaserISF_InterfacesLib FaserMCTruthBaseLib )

# Component(s) in the package:
atlas_add_component( G4FaserAlg
                     src/components/*.cxx
                     NO_PUBLIC_HEADERS
                     INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS}
                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${EIGEN_LIBRARIES} ${XERCESC_LIBRARIES} ${CLHEP_LIBRARIES} AtlasHepMCLib AthenaBaseComps AthenaKernel GaudiKernel G4AtlasInterfaces G4FaserAlgLib G4AtlasAlgLib SGTools StoreGateLib SGtests EventInfo GeneratorObjects FaserMCTruthBaseLib )

atlas_add_test( G4FaserAlgConfig_TestFaser
                SCRIPT python ${CMAKE_CURRENT_SOURCE_DIR}/test/G4FaserAlgConfigNew_Test.py GeoModel.FaserVersion="'FASER-01'" IOVDb.GlobalTag="'OFLCOND-FASER-01'" Output.HITSFileName='faser.HITS.pool.root'
                PROPERTIES TIMEOUT 300 
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

atlas_add_test( G4FaserAlgConfig_TestFaserNu
                SCRIPT python ${CMAKE_CURRENT_SOURCE_DIR}/test/G4FaserAlgConfigNew_Test.py GeoModel.FaserVersion="'FASERNU-03'" IOVDb.GlobalTag="'OFLCOND-FASER-02'" Output.HITSFileName='faserNu.HITS.pool.root'
                PROPERTIES TIMEOUT 300 
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

atlas_add_test( G4FaserAlgConfig_TestFaserNu_NewField
                SCRIPT python ${CMAKE_CURRENT_SOURCE_DIR}/test/G4FaserAlgConfigNew_Test.py GeoModel.FaserVersion="'FASERNU-03'" IOVDb.GlobalTag="'OFLCOND-FASER-03'" Output.HITSFileName='faserNu.HITS.pool.root'
                PROPERTIES TIMEOUT 300 
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

atlas_add_test( G4FaserAlgConfig_TestTestbeam
                SCRIPT python ${CMAKE_CURRENT_SOURCE_DIR}/test/G4FaserAlgConfigNew_Test.py GeoModel.FaserVersion="'FASER-TB00'" IOVDb.GlobalTag="'OFLCOND-FASER-TB00'" Output.HITSFileName='tb.HITS.pool.root'
                PROPERTIES TIMEOUT 300 
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

# Install files from the package:
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
